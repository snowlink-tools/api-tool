<?php

declare(strict_types=1);

/**
 * 辅助库: DB辅助工具
 */

use Snowlink\ApiTool\PkgConfig\ConstantModule\CustomConstant\PkgPageConstant;




if (!function_exists('create_page_info')) {
    /**
     * 生成分页页码详情
     */
    function create_page_info(
        $resource = null,
        int $pageIndex = PkgPageConstant::FIRST,
        int $pageSize=PkgPageConstant::SIZE
    ): array {
        $single = [];
        if ($resource) {
            $length = $resource->count() ?? 0;
            $totalLog = $resource->total() ?? 0;
            $maxPage = $pageSize > 0 ? intval(ceil($totalLog / $pageSize)) : 0;
            $hasMorePages = $resource->hasMorePages();

            $single = [
                'has_more_pages' => $hasMorePages, // 是否有下一页(更多页)
                'total_log' => $totalLog, // 所有记录总数
                'max_page' => $maxPage, // 所有页数
                'page' => $pageIndex,
                'limit' => $pageSize,
                'length' => $length, // 当前页总数目
            ];
        }
        return $single;
    }
}



#