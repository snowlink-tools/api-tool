<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgConfig\ConstantModule\CustomConstant;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 常量: 分页常亮
 * @date 2023-01-11 25:21:02
 * 
 * @Constants
 */
class PkgPageConstant extends AbstractConstants
{

    /**
     * 默认页
     */
    public const FIRST = 1;

    /**
     * 默认分页条数
     */
    public const SIZE = 10;


    #
}