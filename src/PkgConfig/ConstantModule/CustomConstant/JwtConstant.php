<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgConfig\ConstantModule\CustomConstant;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 常量: pkg包提供的基础所需常量
 * @date 2023-01-11 10:21:02
 * 
 * @Constants
 */
class JwtConstant extends AbstractConstants
{

    /**
     * jwt配置: 开启jwt调试模式
     */
    public const JWT_DEBUG_STATUS = false;

    /**
     * jwt配置: jwt默认token_key
     */
    public const JWT_TOKEN_KEY = 'hiG8DlOKvtih6AxlZn5XKImZ06yu8I3mkOzaJrEuW8yAv8Jnkw330uMt8AEqQ5LB';

    /**
     * jwt配置: jwt默认应用名app_key
     */
    public const JWT_APP_KEY = 'my_app';

    /**
     * jwt配置: jwt默认私钥private_key
     */
    public const JWT_PRIVATE_KEY = 'mkOzaJrEuW8yAv8Jnk';

    /**
     * jwt配置: jwt默认的主机域名
     */
    public const JWT_APP_HOST = 'myapp.test';

    /**
     * jwt配置: jwt默认过期时间,单位s
     */
    public const JWT_EXPIRED_SECOND = 3600;

    /**
     * jwt提示文案: 请求的uri未被授权
     */
    public const MSG_UNAUTHORIZED_URI = '请求的uri未被授权';

    /**
     * jwt提示文案: 应用未被授权
     */
    public const MSG_UNAUTHORIZED_APP = '应用未被授权';

    /**
     * jwt提示文案: 参数sign为空
     */
    public const MSG_PARAM_EMPTY_SIGN = '参数sign不能为空';

    /**
     * jwt提示文案: 参数key为空
     */
    public const MSG_PARAM_EMPTY_KEY = '参数key不能为空';

    /**
     * jwt提示文案: 参数ts为空
     */
    public const MSG_PARAM_EMPTY_TS = '参数ts不能为空';

    /**
     * jwt提示文案: key为空时无法获取应用配置
     */
    public const MSG_EMPTY_KEY_FOR_APP = 'key为空时无法获取应用配置';

    /**
     * jwt提示文案: 应用的配置不存在
     */
    public const MSG_NOT_CONFIG_FOR_APP = '应用的配置不存在';

    /**
     * jwt提示文案: 签名不正确
     */
    public const MSG_ERROR_SIGN = '签名不正确';

    #
}
