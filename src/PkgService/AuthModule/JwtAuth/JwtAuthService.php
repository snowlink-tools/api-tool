<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgService\AuthModule\JwtAuth;

use Hyperf\Collection\Arr;
use Lcobucci\JWT\Encoding\CannotDecodeContent;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Token\InvalidTokenStructure;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\UnsupportedHeaderFound;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;

/**
 * 应用服务: Jwt校验
 * @date    2023-01-11 09:42:33
 */
class JwtAuthService
{

    /**
     * 多维数组按键key排序
     * @param array $data
     * @return array
     */
    public function deepArrKsort(array $data = []): array
    {
        foreach ($data as $k => $item) {
            $data[$k] = is_array($item) ? $this->deepArrKsort($item) : $item;
        }
        ksort($data);
        return $data;
    }

    /**
     * 将请求的参数加密并生成签名
     *
     * @param array $requestParams
     * @param array $signConfig
     * @param bool $debug
     * @return string|array
     */
    public function createJwtSignature(
        array $requestParams = [],
        array $signConfig = []
    ): string {
        $ts = isset($requestParams['ts']) ? $requestParams['ts'] : 0;
        $formatParams = Arr::except($requestParams, ['key', 'sign', 'ts', 'debug']);
        $formatParams = (array)$formatParams;
        ## 1.参数排序
        $sortParams = $this->deepArrKsort($formatParams);
        ## 2.参数编码
        $encodeParams = http_build_query($sortParams);
        ## 3.参数的加密
        $md5Params = md5($encodeParams);
        ## 配置
        $signPrefix = isset($signConfig['sign_prefix']) ? trim($signConfig['sign_prefix']) : '';
        $signKey = isset($signConfig['sign_key']) ? trim($signConfig['sign_key']) : '';
        $signSecret = isset($signConfig['sign_secret']) ? trim($signConfig['sign_secret']) : '';
        $sign = md5($signPrefix . $signKey . $signSecret . $ts . $md5Params);
        ## 生成的新签名
        return (string)$sign;
    }

    /**
     * 生成jwt的token
     * @return array
     */
    public function createJwtToken(
        int $staffId = 0,
        string $appKey = ''
    ): array {
        $errMsg = '';
        $success = false;
        $token = '';

        $appHost = '';
        $tokenKey = '';
        $publicKey = '';
        $appPrivateKey = '';

        ## getTokenKey 类型必须严格
        $tokenKey = $this->getTokenKey();

        // $appHost = $this->jwtAuthConfigService()->getAppHost();
        // $tokenKey = $this->jwtAuthConfigService()->getTokenKey();
        // var_dump(__METHOD__ . __LINE__);
        // var_dump($tokenKey);
        // $publicKey = $this->jwtAuthConfigService()->getAppKey();
        // $appPrivateKey = $this->jwtAuthConfigService()->getPrivateKey();
        // $expiredTime = $this->jwtAuthBasicService()->createExpiredTime();
        try {
            $tokenBuilder = (new Builder(new JoseEncoder(), ChainedFormatter::default()));
            $algorithm    = new Sha256();
            $token = $tokenBuilder
                ->issuedBy($appHost)
                ->permittedFor($appHost)
                ## 自定义参数
                ->withClaim('created', time()) // 生成时间
                ->withClaim('staff_id', $staffId)  // 用户id
                ->withClaim('public_key', $publicKey)  // 公钥
                ->withClaim('secrect', $appPrivateKey) // 私钥
                ->withClaim('app_host', $appHost) // 主机
                ->getToken($algorithm, $tokenKey);

            $token = $token->toString();
            $success = true;
            $errMsg = PkgConstant::TIP_MSG_SUCCESS;
        } catch (\Exception $e) {
            $success = false;
            ## 生成token出现异常了
            $errMsg = $e->getMessage();
        }
        $resData = [
            'success' => $success,
            'err_msg' => $errMsg,
            'token' => $token,
        ];
        return $resData;
    }

    /**
     * 解析jwt的token
     * @param string $token token
     * @return array token解析后的数据
     */
    public function parseJwtToken(string $oriToken = ''): array
    {
        $res = [];
        $jwtData = [];
        $success = false;
        $errMsg = '';
        $debug = [];
        try {
            $parser = new Parser(new JoseEncoder());
            $jwtParseRes = $parser->parse($oriToken);
            ## 实际数据
            $jwtData['created'] = $jwtParseRes->claims()->get('created');
            $jwtData['staff_id'] = $jwtParseRes->claims()->get('staff_id');
            $jwtData['public_key'] = $jwtParseRes->claims()->get('public_key');
            $jwtData['secrect'] = $jwtParseRes->claims()->get('secrect');
            $jwtData['app_host'] = $jwtParseRes->claims()->get('app_host');

            ## 调试
            $debug['iss'] = $jwtParseRes->claims()->get('iss');
            $debug['aud'] = $jwtParseRes->claims()->get('aud');
            $debug['jti'] = $jwtParseRes->claims()->get('jti');
            $debug['exp'] = $jwtParseRes->claims()->get('exp');
            $debug['iat'] = $jwtParseRes->claims()->get('iat');

            $success = true;
            $errMsg = PkgConstant::TIP_MSG_SUCCESS;
        } catch (CannotDecodeContent | InvalidTokenStructure | UnsupportedHeaderFound $e) {
            ## 解析token出现异常了
            $errMsg = 'token解析失败: ' . $e->getMessage();
        }
        ## 返回
        $res = [
            'success' => $success,
            'err_msg' => $errMsg,
            
            'ori_token' => $oriToken,
            'jwt_data' => $jwtData,
        ];
        ## 调试模式
        // if ($this->jwtAuthConfigService()->getDebugStatus()) {
        //     $res['debug'] = $debug;
        // }
        return (array)$res;
    }

    /**
     * 生成过期的时间戳
     */
    public function createExpiredTime()
    {
        // $expiredSecond = $this->jwtAuthConfigService()->getExpiredSecord();
        $expiredSecond = 3200;
        $expiredTime = time() + $expiredSecond;
        return $expiredTime;
    }

    /**
     * jwt必备配置: 获取token key
     */
    public function getTokenKey()
    {
        ## tokenKey 必须是44位长度
        $tokenKey = 'YWFhc0pOUnjkd0RLSkJITktKU0RiamhrMTJiM2Joa2pp';
        $tokenKey = trim($tokenKey);
        $key = InMemory::base64Encoded($tokenKey);
        return $key;
    }

    #
}
