<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgMiddleware;

use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Contracts\Arrayable;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Snowlink\ApiTool\PkgService\ResponseModule\PkgResponseTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * 中间件: 底层core重写
 * @date 2023-01-07 14:48:15
 */
class PkgCoreMiddleware extends \Hyperf\HttpServer\CoreMiddleware
{

    /**
     * 统一响应输出
     */
    use PkgResponseTrait;

    /**
     * Handle the response when cannot found any routes.
     *
     * @return array|Arrayable|mixed|ResponseInterface|string
     */
    protected function handleNotFound(ServerRequestInterface $request):mixed
    {
        ## 重写路由找不到的处理逻辑
        $resStr = $this->failureResponseStr(PkgConstant::TIP_MSG_ERROR_404);
        ## 构建json响应返回
        return $this->response()
            ->withAddedHeader('content-type', PkgConstant::SYSTEM_JSON_HEADER)
            ->withBody(new SwooleStream($resStr));
    }

    /**
     * Handle the response when the routes found but doesn't match any available methods.
     *
     * @return array|Arrayable|mixed|ResponseInterface|string
     */
    protected function handleMethodNotAllowed(array $methods, ServerRequestInterface $request):mixed
    {
        ## 重写 HTTP 方法不允许的处理逻辑
        $resStr = $this->failureResponseStr(PkgConstant::TIP_MSG_ERROR_405);
        ## 构建json响应返回
        return $this->response()
            ->withAddedHeader('content-type', PkgConstant::SYSTEM_JSON_HEADER)
            ->withBody(new SwooleStream($resStr));
    }

    #
}
