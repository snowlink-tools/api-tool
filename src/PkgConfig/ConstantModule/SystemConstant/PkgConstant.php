<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 常量: pkg包提供的基础所需常量
 * @date 2023-01-11 10:21:02
 * 
 * @Constants
 */
class PkgConstant extends AbstractConstants
{

    /**
     * 系统常量: 调试标识键名
     */
    public const SYSTEM_DEBUG_KEY_NAME = 'PKG_OPEN_DEBUG_MODEL';

    /**
     * 系统常量: 调试标识1**
     */
    public const SYSTEM_DEBUG_TAG = '6412121cbb2dc2cb9e460cfee7046be2';

    /**
     * 系统常量: json头信息
     */
    public const SYSTEM_JSON_HEADER = 'application/json; charset=utf-8';

    /**
     * 错误码: 500服务端出错
     */
    public const ERROR_CODE_SERVER_ERROR = 500;

    /**
     * 错误码: 200正常
     */
    public const ERROR_CODE_SERVER_OK = 200;

    /**
     * 错误码: 1成功
     */
    public const ERROR_CODE_OK = 1;

    /**
     * 错误码: -1失败
     */
    public const ERROR_CODE_FAIL = -1;

    /**
     * 提示文案: 成功
     */
    public const TIP_MSG_SUCCESS = "成功";

    /**
     * 提示文案: error
     */
    public const TIP_MSG_ERROR = "error";

    /**
     * 提示文案: ok
     */
    public const TIP_MSG_OK = "ok";

    /**
     * 提示文案: 失败
     */
    public const TIP_MSG_FAIL = "失败";

    /**
     * 提示文案: 404
     */
    public const TIP_MSG_ERROR_404 = "Page Not Found";

    /**
     * 提示文案: 405
     */
    public const TIP_MSG_ERROR_405 = "Method Not Allow";

    /**
     * 提示文案: SYSTEM_ERROR 服务端出错
     */
    public const TIP_MSG_SYSTEM_ERROR = "Internal Server Error.";




    #
}
