<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgController;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Psr\Container\ContainerInterface;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Snowlink\ApiTool\PkgService\ResponseModule\PkgResponseTrait;

/**
 * 控制器: 基础控制器抽象类
 * @date 2023-01-12 09:30:08
 */
abstract class PkgAbstractController
{

    #[Inject]
    protected ContainerInterface $container;

    #[Inject]
    protected RequestInterface $request;

    #[Inject]
    protected ResponseInterface $response;

    /**
     * 统一响应输出
     */
    use PkgResponseTrait;

    /**
     * 失败时的响应输出
     * @param string $msg 提示文案
     * @param mixed $single 单记录数据
     * @param array $debug  调试数据
     * @return array 
     */
    public function failure(
        string $msg = PkgConstant::TIP_MSG_FAIL,
        $single = null,
        array $debug = []
    ) {
        return $this->failureResponse($msg, $single, $debug);
    }

    /**
     * 成功时的响应输出(手动拆分单记录与多记录数据)
     * @param mixed $single 单记录数据
     * @param array $lists  多记录数据
     * @param array $debug  调试数据
     * @param string $msg 提示文案
     * @return array
     */
    public function success(
        $single = null,
        array $lists = [],
        array $debug = [],
        string $msg = PkgConstant::TIP_MSG_SUCCESS
    ) {
        return $this->successResponse($single, $lists, $debug, $msg);
    }

    /**
     * 响应输出(响应,自动拆分数据,成功状态下)
     * @param array $response 单记录数据
     * @return array
     */
    public function output(
        array $response = []
    ) {
        ## 自动拆分数据
        $single = $response['single'] ?? null;
        $lists = $response['lists'] ?? [];
        $debug = $response['debug'] ?? [];
        $msg = $response['msg'] ?? PkgConstant::TIP_MSG_SUCCESS;
        ## 类型转换
        $single = (object)$single;
        $lists = (array)$lists;
        $debug = (array)$debug;
        $msg = (string)$msg;
        return $this->successResponse($single, $lists, $debug, $msg);
    }

    #
}
