<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgException\PkgHandler;

use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Validation\ValidationException;
use Hyperf\Validation\ValidationExceptionHandler as BaseValidationExceptionHandler;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Snowlink\ApiTool\PkgService\ResponseModule\PkgResponseTrait;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 异常处理: 验证器
 * @date 2023-01-12 09:46:21
 */
class PkgValidationExceptionHandler extends BaseValidationExceptionHandler
{

    /**
     * 统一响应输出
     */
    use PkgResponseTrait;

    /**
     * 验证器的异常处理
     * @param Throwable|ValidationException $throwable
     * @return mixed|ResponseInterface
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        ## 返回业务提示
        $errMsg = $throwable->validator->errors()->first();
        $errMsg = (string)$errMsg;

        ## 验证器的调试数据
        $debug = [];
        if ($errMsg) {
            $validatorErrors = $throwable->validator->errors()->getMessages();
            $debug = [
                'validator_errors' => $validatorErrors,
            ];
        }
        $resStr = $this->failureResponseStr($errMsg, [], $debug);

        ## 构建json响应返回
        return $response
            ->withStatus(PkgConstant::ERROR_CODE_SERVER_OK) // 错误码:200
            ->withAddedHeader('Content-Type', PkgConstant::SYSTEM_JSON_HEADER)
            ->withBody(new SwooleStream($resStr));
    }

    #
}
