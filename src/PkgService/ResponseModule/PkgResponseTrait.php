<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgService\ResponseModule;

use Hyperf\Context\Context;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;

/**
 * 统一响应输出:json格式
 * @date 2023-01-07 11:51:21
 */
trait PkgResponseTrait
{

    /**
     * 失败时的响应输出
     * @param string $msg 提示文案
     * @param mixed $single 单记录数据
     * @param array $debug  调试数据
     * @return array 返回数组
     */
    public function failureResponse(
        string $msg = PkgConstant::TIP_MSG_FAIL,
        $single = null,
        array $debug = []
    ) {
        return $this->buildBodyData($single, [], false, PkgConstant::ERROR_CODE_FAIL, $msg, $debug);
    }

    /**
     * 失败时的响应输出(返回字符串)
     * @param string $msg 提示文案
     * @param mixed $single 单记录数据
     * @param array $debug  调试数据
     * @return string 返回字符串
     */
    public function failureResponseStr(
        string $msg = PkgConstant::TIP_MSG_FAIL,
        $single = null,
        array $debug = []
    ) {
        $res = $this->buildBodyData($single, [], false, PkgConstant::ERROR_CODE_FAIL, $msg, $debug);
        return (string)json_encode($res); // 数组转为字符串
    }

    /**
     * 成功时的响应输出
     * @param mixed $single 单记录数据
     * @param array $lists  多记录数据
     * @param array $debug  调试数据
     * @param string $msg 提示文案
     * @return array  返回数组
     */
    public function successResponse(
        $single = null,
        array $lists = [],
        array $debug = [],
        string $msg = PkgConstant::TIP_MSG_SUCCESS
    ) {
        return $this->buildBodyData($single, $lists, true, PkgConstant::ERROR_CODE_OK, $msg, $debug);
    }

    /**
     * 基础响应输出
     * @param mixed $single 单记录数据
     * @param array $lists  多记录数据
     * @param boolean $success 成功状态
     * @param integer $code  错误码
     * @param string $msg 提示文案
     * @param array $debug  调试数据
     * @return array  返回数组
     */
    public function buildBodyData(
        $single = null,
        array $lists = [],
        bool $success = true,
        int $code = PkgConstant::ERROR_CODE_OK,
        string $msg = PkgConstant::TIP_MSG_SUCCESS,
        array $debug = []
    ) {
        ## 响应数据内容
        $responseData = [
            'single' => $single,
            'lists' => $lists,
        ];
        return $this->buildStandardResponseData($responseData, $success, $code, $msg, $debug);
    }

    /**
     * json响应输出
     * @param array $oriResponseData 相应数据
     * @param boolean $success 成功状态
     * @param integer $code  错误码
     * @param string $msg  提示文案
     * @param array $debug 调试数据
     * @return array  返回数组
     */
    public function buildStandardResponseData(
        array $oriResponseData = [],
        bool $success = true,
        int $code = PkgConstant::ERROR_CODE_OK,
        string $msg = PkgConstant::TIP_MSG_OK,
        array $debugData = []
    ): array {
        ## 响应内容
        $res = [
            'success' => $success,
            'code' => $code,
            'msg' => $msg,
            'response' => $oriResponseData,
        ];
        ## 调试数据 (从协程里读取调试模式)
        $pkgOpenDebugModel = Context::get(PkgConstant::SYSTEM_DEBUG_KEY_NAME);
        if ($pkgOpenDebugModel) {
            $res['debug'] = $debugData;
        }

        ## 最终需响应的统一格式的数据
        return $res;
    }



    #
}
