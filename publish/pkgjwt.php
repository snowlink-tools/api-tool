<?php

/**
 * 配置: pkgjwt
 * @date 2023-01-13 08:16:22
 */

return [
    ## jwt启用的应用列表
    'allow_apps' => [
        'default',  // 默认应用
    ],
    'auth_apps' => [
        ## default 默认应用
        'default' => [
            'valid_status' => env('JWT_VALID_STATUS', true),
            'token_key' => env('JWT_TOKEN_KEY', 'hiG8DlOKvtih6AxlZn5XKImZ06yu8I3mkOzaJrEuW8yAv8Jnkw330uMt8AEqQ5LB'),
            'app_key' => env('JWT_APP_KEY', 'my_app'),
            'private_key' => env('JWT_PRIVATE_KEY', 'mkOzaJrEuW8yAv8Jnk'),
            'expired_second' => env('JWT_EXPIRED_SECOND', 3600),
            'app_host' => env('JWT_APP_HOST', 'myapp.test'),
            'debug_status' => env('JWT_DEBUG_STATUS', false),
        ],
    ],
    'sign' => [
        'sign_prefix' => env('JWT_SIGN_PREFIX', 'jwt_pre'),
        'sign_key' => env('JWT_SIGN_KEY', 'jwt_app'),
        'sign_secret' => env('JWT_SIGN_SECRET', 'kd8U0ntSsFR'),
    ],

    ## 公共白名单 (无需jwt授权校验和sign校验)
    'auth_whitelist' => [
        '/',
        '/index/index',
        '/index/error',
        '/index/getToken',
    ],

];
