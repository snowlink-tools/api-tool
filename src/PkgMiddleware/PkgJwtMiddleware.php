<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgMiddleware;

use Hyperf\Contract\ConfigInterface;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponseInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Snowlink\ApiTool\PkgConfig\ConstantModule\CustomConstant\JwtConstant;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Snowlink\ApiTool\PkgService\AuthModule\JwtAuth\JwtAuthService;
use Snowlink\ApiTool\PkgService\ResponseModule\PkgResponseTrait;

/**
 * 中间件: jwt校验
 * @date 2023-01-13 09:22:49
 */
class PkgJwtMiddleware implements MiddlewareInterface
{

    /**
     * 统一响应输出
     */
    use PkgResponseTrait;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var HttpResponse
     */
    protected $response;

    /**
     * @var JwtAuthService
     */
    protected function jwtAuthService()
    {
        return new JwtAuthService();
    }

    /**
     * 构造
     */
    public function __construct(
        ContainerInterface $container,
        HttpResponseInterface $response,
        RequestInterface $request
    ) {
        $this->container = $container;
        $this->response = $response;
        $this->request = $request;
    }

    /**
     * process
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $debug = [];
        $success = true;
        $errMsg = "";
        ## a.获取key
        $key = $this->request->input('key', '');
        $key = (string)$key;
        $key = trim($key);
        ## b.是否启用jwt签名验证
        if ($this->needCheckJwtAuth()) {
            ## c.获取jwt配置
            $jwtConfigData = $this->getJwtAppConfig($key);
            $configSuccess = $jwtConfigData['success'] ?? false;
            $whiteListData = $jwtConfigData['whitelist'] ?? [];
            $authUriData = $jwtConfigData['auth_uri'] ?? [];
            $whiteListData = (array)$whiteListData;
            $configErrMsg = $jwtConfigData['err_msg'] ?? PkgConstant::TIP_MSG_ERROR;
            $currentPath = $this->request->getUri()->getPath();
            if ($currentPath) {
                $currentPath = rtrim($currentPath, '/');
            }
            if (!$currentPath) {
                $currentPath = '/';
            }
            $token = $this->request->input('token', '');
            $paramSign = $this->request->input('sign', '');
            $ts = $this->request->input('ts');
            $requestParams = $this->request->all();
            $token = (string)$token;
            $paramSign = (string)$paramSign;
            $paramSign = trim($paramSign);
            $token = trim($token);
            $ts = intval($ts);
            ## debug
            $debug = [
                'right_sign' => '',
                'request_jwt' => [
                    'key' => $key,
                    'token' => $token,
                    'sign' => $paramSign,
                    'ts' => $ts,
                ],
                'request_uri' => $currentPath,
                'request_param' => $requestParams,
            ];
            ## uri白名单
            if (!in_array($currentPath, $whiteListData)) {
                ## 不在白名单时,需要校验
                if (!$key) {
                    $success = false;
                    ## key为空
                    $errMsg = JwtConstant::MSG_PARAM_EMPTY_KEY;
                } else if (!$ts) {
                    $success = false;
                    ## ts为空
                    $errMsg = JwtConstant::MSG_PARAM_EMPTY_TS;
                } else if (!$paramSign) {
                    $success = false;
                    ## sign为空
                    $errMsg = JwtConstant::MSG_PARAM_EMPTY_SIGN;
                } else if (!$configSuccess) {
                    ## 应用配置错误提示
                    $success = false;
                    $errMsg = $configErrMsg;
                } else {
                    $needCheckSignPass = true; // sign签名校验通过后才可以校验授权名单
                    ## d.校验sign
                    if ($this->needCheckJwtSingature()) {
                        ## e1. 接收sign签名参数
                        $signConfig = $this->getJwtSignConfig();
                        $encryptSign =  $this->jwtAuthService()->createJwtSignature($requestParams, $signConfig);
                        ## e2. 生成签名
                        if ($encryptSign != $paramSign) {
                            $success = false;
                            $needCheckSignPass = false;
                            ## 签名不正确
                            $errMsg = JwtConstant::MSG_ERROR_SIGN;
                            $debug['right_sign'] = $encryptSign;
                        }
                    }
                    if ($needCheckSignPass) {
                        ## e.查询出该应用下已授权的uri, 需要校验授权名单
                        if (!in_array($currentPath, $authUriData)) {
                            ## 请求的uri未被授权
                            $success = false;
                            $errMsg = JwtConstant::MSG_UNAUTHORIZED_URI;
                        }
                    }
                }
            }
            //  else {
            //     ## 白名单uri 无需处理
            // }
            ## 抛出异常
            if (!$success) {
                $errMsg = (string)$errMsg;
                $resStr = $this->failureResponseStr($errMsg, [], $debug);
                ## 构建json响应返回
                return $this->response
                    ->withStatus(PkgConstant::ERROR_CODE_SERVER_OK) // 错误码:200
                    ->withAddedHeader('Content-Type', PkgConstant::SYSTEM_JSON_HEADER)
                    ->withBody(new SwooleStream($resStr));
            }
        }
        return $handler->handle($request);
    }

    /**
     * 启用jwt签名验证 : 默认开启
     */
    public function needCheckJwtSingature(): bool
    {
        $enableStatus = env('APP_CHECK_JWT_SIGNATURE', true);
        $res = $enableStatus ? true : false;
        return $res;
    }

    /**
     * 启用jwt应用验证 : 默认开启
     */
    public function needCheckJwtAuth(): bool
    {
        $enableStatus = env('APP_CHECK_JWT_AUTH', true);
        $res = $enableStatus ? true : false;
        return $res;
    }

    /**
     * 获取所有的jwt应用配置
     */
    public function getJwtAppConfig(string $appName = ''): array
    {
        $success = true;
        $errMsg = '';
        $currentAppData = $allowApp =  $whiteListData = $appAuthUriList =  [];
        $appName = trim($appName);
        try {
            $configObj = $this->container->get(ConfigInterface::class);
            if ($appName) {
                $currentAppData = $configObj->get("pkgjwt.auth_apps.{$appName}");
                ## 默认分组
                $defaultAuthUriList = $configObj->get("pkgjwt_auth_uri.default");
                ## 当前app应用分组
                $appAuthUriList = $configObj->get("pkgjwt_auth_uri.{$appName}");
                $defaultAuthUriList = (array)$defaultAuthUriList;
                $appAuthUriList = (array)$appAuthUriList;
                if (is_array($defaultAuthUriList) && is_array($appAuthUriList)) {
                    $appAuthUriList = array_merge($defaultAuthUriList, $appAuthUriList);
                }
            }
            $allowApp = $configObj->get('pkgjwt.allow_apps');
            $whiteListData = $configObj->get('pkgjwt.auth_whitelist');
            ## 格式化
            $currentAppData = (array)$currentAppData;
            $allowApp = (array)$allowApp;
            $whiteListData = (array)$whiteListData;
            ## 校验
            if (!$appName) {
                ## key为空时无法获取应用配置
                $success = false;
                $errMsg = JwtConstant::MSG_EMPTY_KEY_FOR_APP;
            } else if (!in_array($appName, $allowApp)) {
                ## 应用未被授权
                $success = false;
                $errMsg = JwtConstant::MSG_UNAUTHORIZED_APP;
            } else if (empty($currentAppData)) {
                ## 应用的配置不存在
                $success = false;
                $errMsg = JwtConstant::MSG_NOT_CONFIG_FOR_APP;
            }
            ## 白名单过滤唯一性
            if ($whiteListData) {
                $whiteListData = array_filter($whiteListData);
                $whiteListData && $whiteListData = array_unique($whiteListData);
            }
            ## 授权uri过滤唯一性
            if ($appAuthUriList) {
                $appAuthUriList = array_filter($appAuthUriList);
                $appAuthUriList && $appAuthUriList = array_unique($appAuthUriList);
            }
        } catch (\Exception $e) {
            $success = false;
            $errMsg = 'jwt配置有误:' . $e->getMessage();
        }
        $configData = [
            'success' => $success,
            'err_msg' => $errMsg,
            'allows' => $allowApp,
            'app_name' => $appName,
            'app' => $currentAppData,
            'auth_uri' => $appAuthUriList,
            'whitelist' => $whiteListData,
        ];
        return $configData;
    }

    /**
     * 获取jwt签名校验的sign配置
     */
    public function getJwtSignConfig(): array
    {
        $signPrefix = '';
        $signKey = '';
        $signSecret = '';
        try {
            $configObj = $this->container->get(ConfigInterface::class);
            $jwtSignConfig = $configObj->get('pkgjwt.sign');
            $signPrefix = isset($jwtSignConfig['sign_prefix']) ? trim($jwtSignConfig['sign_prefix']) : '';
            $signKey = isset($jwtSignConfig['sign_key']) ? trim($jwtSignConfig['sign_key']) : '';
            $signSecret = isset($jwtSignConfig['sign_secret']) ? trim($jwtSignConfig['sign_secret']) : '';
            ## 成功状态
            $success = true;
            $errMsg = '';
        } catch (\Exception $e) {
            $success = false;
            $errMsg = 'jwt-sign配置有误:' . $e->getMessage();
            ## 暂未做处理
        }
        $signConfig = [
            'success' => $success,
            'err_msg' => $errMsg,
            'sign_prefix' => $signPrefix,
            'sign_key' => $signKey,
            'sign_secret' => $signSecret,
        ];
        return $signConfig;
    }


    #
}
