<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgException\PkgHandler;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Snowlink\ApiTool\PkgException\AppException;
use Snowlink\ApiTool\PkgService\ResponseModule\PkgResponseTrait;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 异常捕捉: 应用错误(stdout标准输出)
 * @date 2023-01-07 11:51:21
 */
class PkgAppExceptionHandler extends ExceptionHandler
{
    /**
     * 统一响应输出
     */
    use PkgResponseTrait;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    public function __construct(StdoutLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * handle
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        ## 错误内容
        $errMsg = $throwable->getMessage();

        ## 输入到日志文件
        $this->logger->error(sprintf('%s[%s] in %s', $errMsg, $throwable->getLine(), $throwable->getFile()));

        ## JSON响应输出错误信息
        $this->logger->error($throwable->getTraceAsString());

        ## 调试模式
        $debug = [
            'err_msg' => $errMsg,
            'err_line' => $throwable->getLine(),
            'err_file' => $throwable->getFile(),
        ];
        $resStr = $this->failureResponseStr($errMsg, [], $debug);

        ## 处理业务提示
        if ($throwable instanceof AppException) {
            ## 属于用户自定义的'应用业务错误' , 错误码:200
            $statusCode = PkgConstant::ERROR_CODE_SERVER_OK; // 错误码:200
        } else {
            $statusCode = PkgConstant::ERROR_CODE_SERVER_ERROR;
        }

        ## 返回
        return $response
            ->withStatus($statusCode)
            ->withAddedHeader('content-type', PkgConstant::SYSTEM_JSON_HEADER)
            ->withBody(new SwooleStream($resStr));
    }

    /**
     * 是否生效
     */
    public function isValid(Throwable $throwable): bool
    {
        return true;
    }


    #
}
