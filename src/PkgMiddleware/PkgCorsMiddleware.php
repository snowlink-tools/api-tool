<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgMiddleware;

use Hyperf\Context\Context;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponseInterface;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * 中间件: 处理跨域请求
 * @date 2023-01-07 15:06:41
 */
class PkgCorsMiddleware implements MiddlewareInterface
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var HttpResponse
     */
    protected $response;

    /**
     * 构造
     */
    public function __construct(
        ContainerInterface $container,
        HttpResponseInterface $response,
        RequestInterface $request
    ) {
        $this->container = $container;
        $this->response = $response;
        $this->request = $request;
    }

    /**
     * process
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        ## 增加header头信息,支持跨域
        $response = Context::get(ResponseInterface::class);
        $response = $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'DNT,Keep-Alive,User-Agent,Cache-Control,Content-Type,Authorization');
        Context::set(ResponseInterface::class, $response);

        ## 通过 get / post 获取debug参数, 判断是否启用调试模式 (写入协程里)
        $debugTagValue = $this->request->input('debug', '');
        $debugTagValue = (string)$debugTagValue;
        $debugTagValue = trim($debugTagValue);
        if (!$debugTagValue) {
            $debugTagValue = $postParams['debug'] ?? '';
        }
        if ($debugTagValue && (md5($debugTagValue) == PkgConstant::SYSTEM_DEBUG_TAG)) {
            Context::set(PkgConstant::SYSTEM_DEBUG_KEY_NAME, true);
        }

        ## OPTIONS 
        if ($request->getMethod() == 'OPTIONS') {
            return $response;
        }

        return $handler->handle($request);
    }

    #
}
