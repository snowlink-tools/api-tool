<?php

declare(strict_types=1);

/**
 * 辅助库: 函数
 */

if (!function_exists('router_autoloader')) {
    /**
     * 自动加载路由
     * @desc 路由文件名后缀统一格式为 xxx.route.php
     * @param string $path
     */
    function router_autoloader(string $path)
    {
        $subRouteFiles = glob(rtrim($path, '/') . '/*.route.php');
        foreach ($subRouteFiles as $subRouteFile) {
            include_once $subRouteFile;
        }
    }
}

if (!function_exists('send_get')) {
    /**
     * 发送 get请求
     */
    function send_get(string $sendUrl = ''): array
    {
        $success = true;
        # 初始化
        $curl = curl_init();
        # 设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $sendUrl);

        # 设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);

        # 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        # 执行命令
        $data = curl_exec($curl);
        ## 状态码
        $curlInfo = curl_getinfo($curl);
        $httpCode = $curlInfo['http_code'] ?? 0;
        $httpCode = intval($httpCode);
        ## 获取到错误信息
        $errMsg = curl_errno($curl);
        if ($errMsg) {
            $errMsg = false;
        }
        # 关闭URL请求
        curl_close($curl);
        # 返回
        $resData = [
            'success' => $success,
            'err_msg' => $errMsg,
            'data' => $data,
            'debug' => [
                'url' => $sendUrl,
                'http_code' => $httpCode,
            ]
        ];
        return $resData;
    }
}

if (!function_exists('send_post')) {
    /**
     * 发送 post 请求
     */
    function send_post(
        string $sendUrl = '',
        array $postData = []
    ) {
        $success = true;
        # 初始化
        $curl = curl_init();
        # 设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $sendUrl);
        # 设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        # 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        # 设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        # 设置post数据
        if ($postData) {
            $postDataStr = json_encode($postData, JSON_UNESCAPED_UNICODE);
        } else {
            $postDataStr = "{}";
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $postDataStr);
        # 执行命令
        $data = curl_exec($curl);
        ## 状态码
        $curlInfo = curl_getinfo($curl);
        $httpCode = $curlInfo['http_code'] ?? 0;
        $httpCode = intval($httpCode);
        ## 获取到错误信息
        $errMsg = curl_errno($curl);
        if ($errMsg) {
            $errMsg = false;
        }
        # 关闭URL请求
        curl_close($curl);

        # 返回
        $resData = [
            'success' => $success,
            'err_msg' => $errMsg,
            'data' => $data,
            'debug' => [
                'url' => $sendUrl,
                'http_code' => $httpCode,
            ]
        ];
        return $resData;
    }
}
