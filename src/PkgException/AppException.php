<?php

declare(strict_types=1);

namespace Snowlink\ApiTool\PkgException;

use Hyperf\Server\Exception\ServerException;
use Snowlink\ApiTool\PkgConfig\ConstantModule\SystemConstant\PkgConstant;

/**
 * 异常捕捉: 业务异常
 * @date 2023-01-07 16:47:56
 */
class AppException extends ServerException
{

    /**
     * 构造
     */
    public function __construct(
        string $msg = PkgConstant::TIP_MSG_FAIL
    ) {
        parent::__construct($msg);
    }

    #
}
